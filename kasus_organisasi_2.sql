CREATE TABLE company(
    id INT PRIMARY KEY AUTO_INCREMENT,
    nama VARCHAR(255),
    alamat VARCHAR(255)
);

CREATE TABLE employee(
    id INT PRIMARY KEY AUTO_INCREMENT,
    nama VARCHAR(255),
    atasan_id INT,
    company_id INT,
    CONSTRAINT FK_atasan FOREIGN KEY (atasan_id)
        REFERENCES employee(id),
    CONSTRAINT FK_company FOREIGN KEY (company_id)
        REFERENCES company(id)
);

INSERT INTO company(nama, alamat) VALUES
("PT JAVAN", "Sleman"), ("PT Dicoding", "Bandung");

INSERT INTO employee(nama, atasan_id, company_id) VALUES
("Pak Budi", NULL, "1"), ("Pak Tono", "1", "1"), ("Pak Totok", "1", "1"), ("Bu Sinta", "2", "1"), ("Bu Novi", "3", "1"),
("Andre", "4", "1"), ("Dono", "4", "1"), ("Ismir", "5", "1"), ("Anto", "5", "1");

SELECT nama FROM employee WHERE atasan_id IS NULL;
SELECT nama FROM employee WHERE id IN
    (SELECT id FROM employee WHERE id
    EXCEPT SELECT atasan_id FROM employee);
SELECT nama FROM employee WHERE atasan_id=1;
SELECT nama FROM employee WHERE atasan_id > 1 AND atasan_id < 4;
SELECT nama, COUNT(id) AS "Jumlah Bawahan" FROM employee WHERE id IN(atasan_id);

SELECT e1.nama , COUNT(*) as "Jumlah Bawahan" FROM employee e1
INNER JOIN employee e2 ON e1.id=e2.atasan_id
GROUP BY e1.id;